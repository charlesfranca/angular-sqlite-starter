import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  constructor(
    private platform: Platform,
    private sqlite: SQLite
  ) {
    platform.ready().then(() => {
      this.openConnection().then(db => {
        db.executeSql('create table IF NOT EXISTS movie(title VARCHAR(150), date VARCHAR(32))', [])
          .then(() => {
            console.log('Executed SQL');
          })
          .catch(e => console.log(e));
      });
    });
  }

  openConnection(): Promise<SQLiteObject> {
    // tslint:disable-next-line: no-unused-expression
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: 'sqlite_database.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          resolve(db);
        })
        .catch(e => {
          console.log(e);
          reject(e);
        });
    });
  }

  createMovie(name: string, date: string) {
    return new Promise((resolve, reject) => {
      this.openConnection().then(db => {
        db.executeSql('INSERT INTO movie VALUES (?,?)', [name, date])
          .then(() => {
            resolve();
          }).catch(e => {
            reject(e);
          });
      });
    });
  }

  getMovies() {
    return new Promise((resolve, reject) => {
      this.platform.ready().then(() => {
        this.openConnection().then(db => {
          db.executeSql('SELECT * FROM movie', []).then(rs => {
            const items = [];
            for (let index = 0; index < rs.rows.length; index++) {
              const item = rs.rows.item(index);
              items.push({
                name: item.title,
                date: item.date
              });
            }
            console.log(JSON.stringify(items));
            resolve(items);
          }).catch(e => {
            reject(e);
          });
        });
      });
    });
  }

  getMovie(id) {
    return new Promise((resolve, reject) => {
      this.platform.ready().then(() => {
        this.openConnection().then(db => {
          db.executeSql('SELECT * FROM movie where id = ?', [id]).then(rs => {
            console.log(JSON.stringify(rs.rows.item(0)));
            resolve(rs.rows.item(0));
          }).catch(e => {
            reject(e);
          });
        });
      });
    });
  }

}
