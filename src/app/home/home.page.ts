import { Component, OnInit } from '@angular/core';
import { SqliteService } from '../core/services/sqlite.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  movies: Array<any>;

  constructor(
    private sqliteService: SqliteService
  ) {}

  ngOnInit(): void {
    
  }

  ionViewDidEnter() {
    this.sqliteService.getMovies().then(rs => {
      this.movies = rs as Array<any>;
      alert('has data');
    });
  }

  saveMovie(name: string, date: string) {
    this.sqliteService.createMovie(name, date).then(() => {
      this.sqliteService.getMovies().then(data => {
        this.movies = data as Array<any>;
      });
    });
  }

}
